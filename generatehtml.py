import os
from string import Template

from compare import PDContainer


def generate_html_library(db_object):
    """
    Функция создает html страницу с данными всей библиотеки.
    :param db_object: Объект для доступа к БД.
    :return: None
    """
    line_style_num = 1
    table = ''
    library_obj = db_object.select_all_from_library_ordered()
    with open("res_html_report/table_row.html") as row_templ_file:
        line_template = Template(row_templ_file.read())
    for item in library_obj:
        bitrate_style = "seedmed" if item['bitrate'] >= 320 else "leechmed"
        line = line_template.safe_substitute(line_style_num=line_style_num, db_id=item['id'], artist=item['artist'],
                                             date=item['date'], album=item['album'], tnumber=item['tnumber'],
                                             title=item['title'], length=item['length'], bitrate_style=bitrate_style,
                                             bitrate=item['bitrate'], mime=item['mime'],
                                             file_path=os.path.join(item['path'], item['file']))
        line_style_num = 2 if line_style_num == 1 else 1
        table += line + '\n'
    with open('res_html_report/report_template.html', 'r') as file:
        template = Template(file.read())
        result = template.safe_substitute(table_body=table, total=db_object.get_library_items_count(),
                                          title_string='Аудио библиотека')
        with open('library_report.html', 'w') as target:
            target.write(result)


def generate_compare_report(db_name: str):
    """
    Создание html файла с отчетом по дубликатам в БД
    :param db_name: строка с именем базы (с префиксом)
    :return: None
    """
    line_style_num = 1
    table = ''
    duplicates_container = PDContainer(db_name)
    duplicates_container.prepare_list_of_possible_duplicates()
    duplicates_container.calc_all_pds_result()
    with open("res_html_report/table_row.html") as row_templ_file:
        line_template = Template(row_templ_file.read())
    for pd in duplicates_container.get_next_duplicate():
        for item in pd.media_objects:
            bitrate_style = "seedmed" if item['bitrate'] >= 320 else "leechmed"
            line = line_template.safe_substitute(line_style_num=line_style_num,
                                                 db_id=item['id'], artist=item['artist'],
                                                 date=item['date'], album=item['album'], tnumber=item['tnumber'],
                                                 title=item['title'], length=item['length'],
                                                 bitrate_style=bitrate_style,
                                                 bitrate=item['bitrate'], mime=item['mime'],
                                                 file_path=os.path.join(item['path'], item['file']))
            table += line + '\n'
        line_style_num = 2 if line_style_num == 1 else 1
    with open('res_html_report/report_template.html', 'r') as file:
        template = Template(file.read())
        result = template.safe_substitute(table_body=table, total='-',
                                          title_string='Список дубликатов')
        with open('duplicate_report.html', 'w') as target:
            target.write(result)
