import os.path
import sys

try:
    import sqlalchemy
    from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
    from sqlalchemy.sql import select, func
    from sqlalchemy.sql.expression import alias
except ModuleNotFoundError:
    print("Требуется SQLAlchemy версии не ниже 1.3")
    sys.exit(1)
from classes.hash_object import HashObject
from classes.media_object import MOContaner


class DBDriverNotFound(Exception):
    pass


class DbHandler(object):
    """
    Объект для обращения к БД
    """
    def __init__(self, db_file_path: str, driver='sqlite', drop=False, sqlalchemy_echo=False, readonly=False):
        if os.path.isfile(db_file_path) is False\
                and ':memory:' not in db_file_path\
                and readonly is True:
            raise FileNotFoundError
        if driver == 'sqlite':
            if db_file_path.startswith('sqlite'):
                self.db_path = db_file_path
            else:
                self.db_path = 'sqlite:///' + db_file_path
        else:
            raise DBDriverNotFound
        self.engine = sqlalchemy.create_engine(self.db_path, echo=sqlalchemy_echo)
        self.conn = self.engine.connect()
        self.metadata = MetaData()
        self.library = Table('library', self.metadata,
                             Column('id', Integer, primary_key=True),
                             Column('artist', String),
                             Column('date', String),
                             Column('album', String),
                             Column('title', String),
                             Column('tnumber', Integer, default=0),
                             Column('album_artist', String, nullable=True),
                             Column('path', String, nullable=False),
                             Column('length', Integer, default=0),
                             Column('file', String, nullable=False),
                             Column('bitrate', Integer, default=0),
                             Column('mime', String, default='unknown'),
                             Column('filesize', Integer, default=0),
                             Column('path_to_cue', String))
        self.hashes = Table('hashes', self.metadata,
                            Column('id', Integer, primary_key=True),
                            Column('ext_id', Integer,
                                   ForeignKey("library.id"),
                                   nullable=False),
                            Column('hash', String, nullable=False),
                            Column('type', String, nullable=False))
        if drop is True:
            self.drop_all_tables()
        if not readonly:
            self.metadata.create_all(self.engine)

    def insert_new_media_object_to_db(self, media_object, hash_object_list: list) -> int:
        """
        Insert новой записи в базу
        :param hash_object_list: list c набором объектов хэшей типа HasObject
        :param media_object: объект типа с метаданными для вставки MediaObject
        :return: ID вставленного объекта library
        """
        ins = self.library.insert().values(artist=media_object.artist,
                                           album=media_object.album,
                                           tnumber=media_object.tnumber,
                                           title=media_object.title,
                                           date=media_object.date,
                                           album_artist=media_object.album_artist,
                                           length=media_object.length,
                                           path=media_object.path,
                                           file=media_object.file,
                                           bitrate=media_object.bitrate,
                                           mime=media_object.mime,
                                           filesize=media_object.filesize,
                                           path_to_cue=media_object.path_to_cue)
        key = self.conn.execute(ins).lastrowid
        for ho in hash_object_list:
            ins = self.hashes.insert().values(ext_id=key,
                                              hash=ho.hashed,
                                              type=ho.hash_type)
            self.conn.execute(ins)
        return key

    def get_repeated_objects(self):
        """select
        h2.ext_id, h2.hash, h2.htype
        from hashes as h1
        left join hashes as h2 on h1.ext_id = h2.ext_id
        where h1.hash in (
        SELECT hash from hashes
        group by hash, htype
        having count(*)>1)
        order by h2.type;
        :return объекты дубликатов
        """
        # сделать внутренний select
        duplicate_hashes_inner = select([self.hashes.c.hash]).group_by('hash', 'type').\
            having(func.count(self.hashes.c.hash) > 1)
        # подготовить alias'ы hashes для join
        h1 = alias(self.hashes)
        h2 = alias(self.hashes)
        # подготовка join hashes на саму себя
        hashes_join = h1.join(h2, h1.c.ext_id == h2.c.ext_id)
        # для каждой записи из внутреннего селекта произвести загрузку всех данных записи
        for i in self.conn.execute(duplicate_hashes_inner).fetchall():
            # вытаскиваем данные по уникальным записям library
            """ select * from library
            where id in (
            select ext_id
            from hashes
            where hash = :param
            GROUP by ext_id) """
            hashes_objects = list()
            media_objects = MOContaner()
            # внутренний запрос по получению всех уникальных id записей, имеющих заданных хэш
            duplicate_library_inner = select([self.hashes.c.ext_id]).where(self.hashes.c.hash == i[0]).\
                group_by('ext_id')
            # внешний запрос с получением данных всей library по интересующим id
            duplicate_library_data = select([self.library]).where(self.library.c.id.in_(duplicate_library_inner))
            # создаем под каждую строку MediaObject и записываем в общий лист
            for mo in self.conn.execute(duplicate_library_data).fetchall():
                temp = {k: mo[k] for k in mo.keys()}
                media_objects.add(dict_=temp, id_=mo['id'])
            # если количество записей library == 1 или записи не возвращены, анализировать на дубликаты нечего,
            # надо переходить к следующему совпадению
            if media_objects.get_mo_count() < 2:
                continue
            # делаем запрос по всем хэшам записей, имеющих неуникальные хэши
            duplicate_hashes_data = select([h2.c.ext_id, h2.c.hash, h2.c.type]).select_from(hashes_join).\
                where(h1.c.hash == i[0]).order_by(h2.c.type)
            for ho in self.conn.execute(duplicate_hashes_data).fetchall():
                hashes_objects.append(HashObject.from_db(ext_id=ho['ext_id'], hashed=ho['hash'], hash_type=ho['type']))
            yield media_objects, hashes_objects

    def finish(self):
        """
        Закрытие сессии БД
        """
        self.conn.close()

    def select_all_from_library_ordered(self):
        """
        Выборка всей библиотеки, отсортированная по папкам и номеру трека
        :return: объект с результатам выполнения запроса
        """
        query = select([self.library]).order_by('path', 'tnumber')
        return self.conn.execute(query)

    def select_all_from_library_ordered_offset(self, limit: int, offset: int):
        """
        Выборка всей библиотеки c учетом окна смещения.
        :return: объект с результатам выполнения запроса
        """
        query = select([self.library.c.id,
                        self.library.c.artist,
                        self.library.c.date,
                        self.library.c.album,
                        self.library.c.title,
                        self.library.c.tnumber,
                        self.library.c.mime,
                        self.library.c.bitrate,
                        self.library.c.path,
                        ])\
            .limit(limit).offset(offset)
        return self.conn.execute(query)

    def get_library_items_count(self) -> int:
        """
        Отображение количества записей в библиотеке
        :return: int - количество записей
        """
        query = select([func.count()]).select_from(self.library)
        return self.conn.execute(query).fetchone()[0]

    def get_library_item(self, item_id: int):
        """
        Получение записи бибилотеки по ID
        :param item_id:
        :return:
        """
        q = select([self.library]).select_from(self.library).where(self.library.c.id == item_id)
        return self.conn.execute(q).fetchone()

    def drop_all_tables(self):
        """
        Удаление таблиц library и hashes из имеющейся базы данных.
        :return: Boolean
        """
        self.library.drop(bind=self.engine, checkfirst=True)
        self.hashes.drop(bind=self.engine, checkfirst=True)
