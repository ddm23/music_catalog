from unittest import TestCase

import filefunc


class FilefuncTest(TestCase):
    def setUp(self) -> None:
        pass

    def test_get_track_number(self):
        input_parameter = {}
        result = filefunc.a_get_track_number(input_parameter)
        self.assertEqual(result, 0)
        input_parameter = {'tracknumber': "1/10"}
        result = filefunc.a_get_track_number(input_parameter)
        self.assertEqual(result, 1)
        input_parameter = {'tracknumber': "two/six"}
        result = filefunc.a_get_track_number(input_parameter)
        self.assertEqual(result, 0)
        input_parameter = {'tracknumber': "test"}
        result = filefunc.a_get_track_number(input_parameter)
        self.assertEqual(result, 0)

    def tearDown(self) -> None:
        pass
