from unittest import TestCase

import dbfunc
from classes.hash_object import HashObject
from classes.media_object import MediaObject


class DBTest(TestCase):
    def setUp(self):
        self.obj = dbfunc.DbHandler('sqlite:///', sqlalchemy_echo=False)
        self.instance1 = MediaObject()
        self.instance1.metadata = {'artist': 'Batushka', 'title': 'Yekteniya 1', 'tnumber': '1', 'album': 'Litourgiya',
                                   'date': '2015', 'artist_opt': 'batushka', 'length': 345,
                                   'path': '/Users/Dmitry/music_catalog/temp/Batushka/2015 - Litourgiya',
                                   'file': '01 - Yekteniya 1.flac', 'bitrate': 970, 'mime': 'audio/flac',
                                   'filesize': 41931542}
        self.instance2 = MediaObject(artist="Electric Wizard", album="Time To Die", tnumber=1,
                                     title="Incense For The Damned", date="2014",
                                     length=642, path="/Volumes/My Passport/Music/Electric Wizard/2014 - Time To Die",
                                     file="01. Incense For The Damned.mp3", bitrate=320, mime="audio/mp3",
                                     filesize=25770591)
        self.hash1 = HashObject(str_for_hashing='hash1', hash_type='1')
        self.hash2 = HashObject(str_for_hashing='hash2', hash_type='2')

    def tearDown(self):
        self.obj.finish()

    def test_add_new_items(self):
        result = self.obj.insert_new_media_object_to_db(self.instance1, hash_object_list=[self.hash1, self.hash2])
        self.assertIs(type(result), int)
        result2 = self.obj.insert_new_media_object_to_db(self.instance2, hash_object_list=[self.hash1, self.hash2])
        self.assertIs(type(result2), int)
        self.assertGreater(result2, result)

    def test_select_all_from_library(self):
        self.test_add_new_items()
        result = self.obj.select_all_from_library_ordered().fetchall()
        self.assertGreater(len(result), 0)

    def test_drop_library(self):
        self.test_add_new_items()
        self.obj.drop_all_tables()
