from unittest import TestCase

import cuesheet
from cuesheet import CuesheetReferenceAudioFileNotFound


class DBTest(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_tags_from_cue_line(self):
        test_resource = {'in': "REM DISCID 02096001", 'out': ['discid', "02096001"]}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'REM COMMENT "ExactAudioCopy v1.0b3"', 'out': ['comment', "ExactAudioCopy v1.0b3"]}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'REM GENRE "Melodic Death Metal"', 'out': ['genre', "Melodic Death Metal"]}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'REM DATE 2016', 'out': ['date', '2016']}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'REM unknown tag', 'out': ['unknown', 'unknown tag']}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'REM ONEWORD', 'out': ['unknown', 'ONEWORD']}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'PERFORMER "Insomnium"', 'out': ['artist', 'Insomnium']}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

        test_resource = {'in': 'TRACK 01 AUDIO', 'out': ['cuetnumber', '01 AUDIO']}
        result = cuesheet.get_tags_from_cue_line(test_resource['in'])
        self.assertListEqual(result, test_resource['out'])

    def test_Cuesheet(self):
        path = './materials/1/Batushka - Litourgiya.cue'
        sharedtags_orig = {'genre': 'Black/Doom Metal', 'date': '2015', 'discid': '5809AC08', 'comment': 'ExactAudioCopy v1.0b6', 'artist': 'Batushka', 'title': 'Litourgiya'}
        tracks_orig = [{'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'cuetnumber': '01 AUDIO', 'tracknumber': 1, 'title': 'Yekteniya 1', 'artist': 'Batushka', 'indexes': {'01': '00:00:00'}, 'length': 346}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '02 AUDIO', 'tracknumber': 2, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 2', 'artist': 'Batushka', 'indexes': {'01': '05:45:40'}, 'length': 262}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '03 AUDIO', 'tracknumber': 3, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 3', 'artist': 'Batushka', 'indexes': {'01': '10:08:10'}, 'length': 292}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '04 AUDIO', 'tracknumber': 4, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 4', 'artist': 'Batushka', 'indexes': {'01': '14:59:41'}, 'length': 320}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '05 AUDIO', 'tracknumber': 5, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 5', 'artist': 'Batushka', 'indexes': {'01': '20:19:38'}, 'length': 360}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '06 AUDIO', 'tracknumber': 6, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 6', 'artist': 'Batushka', 'indexes': {'01': '26:19:45'}, 'length': 253}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '07 AUDIO', 'tracknumber': 7, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 7', 'artist': 'Batushka', 'indexes': {'01': '30:32:65'}, 'length': 334}, {'file': 'fake_Batushka - Litourgiya.flac', 'cuefilename': './materials/1/Batushka - Litourgiya.cue', 'cuetnumber': '08 AUDIO', 'tracknumber': 8, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': 'Yekteniya 8', 'artist': 'Batushka', 'indexes': {'01': '36:07:29'}, 'length': -2167}]
        result = cuesheet.Cuesheet(path)
        self.assertDictEqual(result.sharedtags, sharedtags_orig)
        self.assertListEqual(result.tracks, tracks_orig)

        path = "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue"
        sharedtags_orig = {'discid': '02096001', 'comment': 'ExactAudioCopy v1.0b3', 'genre': 'Melodic Death Metal', 'date': '2016', 'artist': 'Insomnium', 'title': "Winter's Gate"}
        tracks_orig = [{'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'cuetnumber': '01 AUDIO', 'tracknumber': 1, 'title': "Winter's Gate, Pt. 1", 'artist': 'Insomnium', 'indexes': {'01': '00:00:00'}, 'length': 375}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '02 AUDIO', 'tracknumber': 2, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 2", 'artist': 'Insomnium', 'indexes': {'01': '06:15:00'}, 'length': 397}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '03 AUDIO', 'tracknumber': 3, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 3", 'artist': 'Insomnium', 'indexes': {'01': '12:51:60'}, 'length': 352}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '04 AUDIO', 'tracknumber': 4, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 4", 'artist': 'Insomnium', 'indexes': {'01': '18:43:40'}, 'length': 321}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '05 AUDIO', 'tracknumber': 5, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 5", 'artist': 'Insomnium', 'indexes': {'01': '24:05:20'}, 'length': 265}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '06 AUDIO', 'tracknumber': 6, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 6", 'artist': 'Insomnium', 'indexes': {'01': '28:30:00'}, 'length': 314}, {'file': "fake_Insomnium - Winter's Gate (Japan).flac", 'cuefilename': "./materials/2/Insomnium - Winter's Gate (Japan) (tracks).cue", 'cuetnumber': '07 AUDIO', 'tracknumber': 7, 'mime': 'unknown', 'bitrate': 0, 'filesize': 0, 'title': "Winter's Gate, Pt. 7", 'artist': 'Insomnium', 'indexes': {'01': '33:43:60'}, 'length': -2024}]
        result = cuesheet.Cuesheet(path)
        self.assertDictEqual(result.sharedtags, sharedtags_orig)
        self.assertListEqual(result.tracks, tracks_orig)

    def test_Cuesheet_Exceptions(self):
        path = "./materials/3/Insomnium - Winter's Gate (Japan) (tracks).cue"
        self.assertRaises(CuesheetReferenceAudioFileNotFound, cuesheet.Cuesheet, path)
