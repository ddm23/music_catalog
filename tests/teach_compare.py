import sys

import commonfunc
from compare import PDContainer

CORRECTION = 0.01


if __name__ == '__main__':
    weights_dict = commonfunc.load_dict_from_config('compare_weights')
    input_correction = input(f'Коррекция (стандартное значение {CORRECTION}):\n')
    try:
        CORRECTION = float(input_correction)
    except ValueError:
        print(f'Значение коррекции не распознано, используется стандартное {CORRECTION}')
    o = PDContainer('sqlite:///database.db')
    o = PDContainer('sqlite:///prod170719.db')
    print('Подготовка списка дубликатов')
    o.prepare_list_of_possible_duplicates()
    print('Список дубликатов подготовлен')
    o.calc_all_pds_result(weights_dict)
    print("Веса посчитаны")
    o.sort_pds_by_result()
    print(f'Стартовые веса: {weights_dict}')
    # for pd in o.pds:
    #     pd.print_data()
    for a in o.get_next_possible_duplicate():
        a.calculate_result(weights=weights_dict)
        a.print_data()
        ask = ''
        while True:
            ask = input('Дубликат? y\\n, e - исключить из обработки, q - выход из цикла\n')
            if ask in ('y', 'n'):
                for key in a.features.keys():
                    if a.features[key] == 1:
                        if ask == 'y':
                            if a.is_duplicate is True:
                                weights_dict[key] += CORRECTION
                            else:
                                weights_dict[key] -= CORRECTION
                        if ask == 'n':
                            if a.is_duplicate is True:
                                weights_dict[key] -= CORRECTION
                            else:
                                weights_dict[key] += CORRECTION
                print(weights_dict)
                break
            if ask in ('e', 'q'):
                break
        try:
            ask = input("Введите действие: n, q, s\n")
        except (EOFError, KeyboardInterrupt):
            print("Для выхода введите q")
        if ask == 'q':
            save = input("Сохранить веса? y - сохранить, любая другая - не сохранять\n")
            if save == 'y':
                commonfunc.save_dict_to_config('compare_weights', saving=weights_dict)
            sys.exit(0)
        elif ask == 's':
            commonfunc.save_dict_to_config('compare_weights', saving=weights_dict)
            print('Веса сохранены')
        elif ask == 'n':
            print('Переход к следующей записи.')  # просто переходим к следующему
