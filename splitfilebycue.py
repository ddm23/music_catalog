import os
import subprocess
import sys

import mutagen

from constants import split_result_output_root_path, INCLUDE_FILENAMES
from cuesheet import Cuesheet

output_codec = 'flac'
overwrite_mode = 'never'
filename_pattern = '%n - %t'
COVERS_EXTENSIONS = ('.jpg', '.png')


class CueSplitFactory(object):
    __instance = None

    @staticmethod
    def get_instance():
        """ Static access method. """
        if CueSplitFactory.__instance is None:
            CueSplitFactory()
        return CueSplitFactory.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if CueSplitFactory.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            CueSplitFactory.__instance = self


class CuesheetHasMoreThanOneFile(Exception):
    def __init__(self, cue, audio_file=''):
        self.error_cue = cue
        self.error_audio_file = audio_file


class TempFolderIsNotEmpty(Exception):
    pass


class SplittedFilesListIsEmpty(Exception):
    pass


def split_file_by_cue(path_to_cue: str, return_files_paths=False):
    cue = Cuesheet(path_to_cue, fix_album_tag=True)
    cue_files_list = cue.get_list_of_filenames()
    if len(cue_files_list) > 1:  # проверка - если в cue больше одного файла, считаем,
        # что образ уже порезан
        raise CuesheetHasMoreThanOneFile
    else:
        # подготовка папки для временных файлов
        output_current_cue_dir = os.path.join(split_result_output_root_path,
                                              cue.sharedtags['artist'],
                                              f"{cue.sharedtags['date']} - {cue.sharedtags['album']}")
        if not os.path.exists(output_current_cue_dir):
            os.makedirs(output_current_cue_dir)
            print("Directory created: " + output_current_cue_dir)
        else:
            print("Directory exists: " + output_current_cue_dir)
            if len(os.listdir(output_current_cue_dir)) > 0:
                print("Directory is not empty. Stop for this file.")
            # raise TempFolderIsNotEmpty

        # вызов утилиты для разрезания файла
        call_external_process_to_split_file_by_cue(path_to_cue,
                                                   os.path.join(os.path.dirname(path_to_cue), cue_files_list[0]),
                                                   output_current_cue_dir)
        # обращение к индексу 0, потому что ожидается один аудио файл

        # проверяем, что список получившихся после разрезания файлов не пустой
        split_result = os.listdir(output_current_cue_dir)
        if len(split_result) is 0:
            print("Сконвертированные файлы не найдены!")
            raise SplittedFilesListIsEmpty

        # записываем теги в сконвертированные файлы
        total_tracks = len(cue.tracks)
        result_files = []
        for i in cue.tracks:
            splitted_file_path = os.path.join(output_current_cue_dir, f'{i["tracknumber"]:02d} - {i["title"]}.flac')
            mutagen_object = mutagen.File(splitted_file_path, easy=True)
            for tag in cue.sharedtags:
                mutagen_object[tag] = cue.sharedtags[tag]
            mutagen_object['title'] = i['title']
            mutagen_object['tracknumber'] = str(i['tracknumber'])
            mutagen_object['totaltracks'] = str(total_tracks)
            mutagen_object.save()
            result_files.append(splitted_file_path)
        if return_files_paths:
            return result_files
        else:
            return 0


def check_cue_folder_for_cover_files(path):
    dir_items = os.listdir(path)
    files = []
    # сортируем найденное на файлы и папки
    for item in dir_items:
        if os.path.isfile(os.path.join(path, item)):
            if not item.startswith('.') \
                    and (item.lower().endswith(COVERS_EXTENSIONS) and item.lower().startswith(INCLUDE_FILENAMES)):
                # убираем файлы, не относящиеся к музыкальным файлам, но оставляем обложки
                # потенциально приводит к пустым папкам, в которых не было музыкальных файлов
                files.append(os.path.join(path, item))
    return files


def call_external_process_to_split_file_by_cue(path_to_cue, path_to_flac, output_path):
    # If you want to run any application without digging around for the executable file in the package
    # (it is not always the same name as the app), use this:
    # subprocess.call(["/bin/bash","-c","open /Applications/Calculator.app"])
    try:
        subprocess.run(['shnsplit',
                        '-f', path_to_cue,
                        '-o', output_codec,
                        '-d', output_path,
                        '-O', overwrite_mode,
                        '-t', filename_pattern,
                        path_to_flac
                        ],
                       check=True)
    except FileNotFoundError:
        print('shnsplit не найден.')
    except PermissionError as e:
        print("Невозможно получить доступ к приложению shnsplit: " + str(e))
    except subprocess.CalledProcessError as e:
        print("Проблема в работе shnsplit: " + str(e))
        print(f'-f "{path_to_cue}" -o "{output_codec}" -d "{output_path}" -O "{overwrite_mode}" -t "{filename_pattern}" "{path_to_flac}"')
    return 0


def remove_temp_files():  # подчищаем временные файлы
    try:
        os.rmdir(split_result_output_root_path)
        print("Removed: " + split_result_output_root_path)
    except OSError as e:
        print("OSError: " + str(e))


if __name__ == "__main__":
    # <script.py> <path_to_cuesheet.cue>
    if len(sys.argv) != 2:
        print(f"Wrong input parameters: {sys.argv}. Exit.")
        sys.exit(1)
    split_file_by_cue(sys.argv[1])
