import dbfunc
from commonfunc import load_dict_from_config

MO_ATTRIBUTES_FOR_CHECK = ('artist', 'album', 'title', 'length', 'tnumber', 'file', 'mime', 'path')
THRESHOLD = 4.0


class ConsoleColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class PDContainer(object):
    def __init__(self, db_name: str):
        self.pds = list()
        self.db_name = db_name

    def calc_all_pds_result(self, wgh: dict = None):
        if wgh is None:
            wgh = load_dict_from_config('compare_weights')
        for pd in self.pds:
            pd.prepare_features()
            pd.calculate_result(wgh)

    def sort_pds_by_result(self):
        self.pds.sort(key=lambda x: x.result, reverse=True)

    def get_next_possible_duplicate(self):
        """
        Генератор, возвращающий вложенные объекты PossibleDuplicate.
        :return: PossibleDuplicate
        """
        for pd in self.pds:
            yield pd

    def get_next_duplicate(self):
        """
        Генератор, возвращающий вложенные объекты PossibleDuplicate, являющихся дубликатами.
        :return: PossibleDuplicate
        """
        for pd in self.pds:
            if pd.is_duplicate:
                yield pd

    def prepare_list_of_possible_duplicates(self):
        db_obj = dbfunc.DbHandler(self.db_name, readonly=True)
        for m, h in db_obj.get_repeated_objects():
            unique_ids = True
            for pd in self.pds:
                if pd.check_for_unique_ids(m.ids) is True:
                    unique_ids = False
            if unique_ids:
                self.pds.append(PossibleDuplicate(media_objs_container=m, hash_objs=h))
        db_obj.finish()


class PossibleDuplicate(object):
    def __init__(self, media_objs_container, hash_objs):
        self.result = 0.0
        self.media_objects = media_objs_container
        self.hash_objects = hash_objs
        self.features = dict()
        self.is_duplicate = False

    def prepare_features(self):
        """
        Подготавливает список фич для текущего объекта
        """
        for attr in MO_ATTRIBUTES_FOR_CHECK:
            self._append_unique_check_result_for_attribute(attr=attr)
        # учет записей, полученных из cue
        unique_htypes = set([ho.hash_type for ho in self.hash_objects])
        for hash_type in unique_htypes:
            self._append_unique_check_result_for_hash_type(hash_type)
        # если все записи одинаковые и не равняются None - перед нами записи из cue, и mime, path и fs
        # анализировать не имеет смысла
        path_to_cue_set = set([mo.path_to_cue for mo in self.media_objects.mo_objs])
        if len(path_to_cue_set) == 1 and all(path_to_cue_set) is None:
            for attr in ['mime', 'path', 'fs']:
                self.features[attr] = 0
        # добавляем вес для фичи, если какого-то значения нет
        for attr in ['artist', 'album', 'title']:
            attr_set = ([mo.attr for mo in self.media_objects.mo_objs])
            if any(attr_set) is None:
                self.features[str(attr+'_none')] = 1
            else:
                self.features[str(attr + '_none')] = 0

    def calculate_result(self, weights: dict):
        """
        Расчет результата на основании полученного словаря с весами
        """
        calc_weight = dict()
        for key in self.features.keys():
            calc_weight[key] = self.features[key] * weights[key]
        self.result = sum(calc_weight[x] for x in calc_weight)
        if self.result >= THRESHOLD:
            self.is_duplicate = True

    def print_data(self):
        for mo_obj in self.media_objects.mo_objs:
            s = ''
            for mo_k in mo_obj.metadata.keys():
                if mo_k in self.features and self.features[mo_k] == 1:
                    s += f'{ConsoleColors.OKBLUE}{mo_k}{ConsoleColors.ENDC}: {ConsoleColors.WARNING}"' \
                        f'{mo_obj.metadata[mo_k]}"{ConsoleColors.ENDC}\t\t '
                else:
                    s += f'{ConsoleColors.OKBLUE}{mo_k}{ConsoleColors.ENDC}: {mo_obj.metadata[mo_k]}\t\t'
            print(s)
        print(self.features)
        print(f"Является дубликатом: {self.is_duplicate}. Результат: {self.result}")

    def _append_unique_check_result_for_attribute(self, attr):
        # все значения одного атрибута из всех MediaObject текущего PossibleDuplicate сохраняются в set через
        # генератор списка, который убирает дубликаты при инициации. В итоге если длина сета == 1 - все значения
        # одинаковые, если больше 1 - значения разные.
        if len(set([mo.get_value_for_compare(attr) for mo in self.media_objects.mo_objs])) == 1:
            self.features[attr] = 1
        else:
            self.features[attr] = 0

    def _append_unique_check_result_for_hash_type(self, htype):
        # все значения одного атрибута из всех HashObject текущего PossibleDuplicate сохраняются в set через
        # генератор списка, который убирает дубликаты при инициации. В итоге если длина сета == 1 - все значения
        # одинаковые, если больше 1 - значения разные.
        if len(set([ho.hashed for ho in self.hash_objects if ho.hash_type == htype])) == 1:
            self.features[htype] = 1
        else:
            self.features[htype] = 0

    def check_for_unique_ids(self, ids_: set):
        return self.media_objects.has_same_ids(ids_)
