import argparse
import logging
import os
import sys
from datetime import datetime

import dbfunc
import filefunc
import generatehtml

try:
    import mutagen  # TODO - сделать проверки для всех импортируемых внешних модулей
except ImportError:
    print("mutagen module is not found. Exit.")
    sys.exit(0)


def log_level_select(arg):
    return {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }.get(arg, logging.WARNING)


def main():
    # подготовка парсера входных параметров
    parser = argparse.ArgumentParser()
    parser.add_argument("--db",
                        help="Filename of the database. 'sqlite:///:memory:' for in-memory sqlite3 mode. Default is "
                             "'database.db'.",
                        default='database.db')
    parser.add_argument("--path", help="Path for scanning. Only one path for work from the commandline.", default=False)
    parser.add_argument("--log", help="Filename for log file. Default is 'log_file.log'.", default=False)
    parser.add_argument("--loglevel",
                        help="Level of logging. Default is 'ERROR'.\nAllowed values:\n1. DEBUG\n2. INFO\n3. "
                             "WARNING\n4. ERROR\n5. CRITICAL",
                        default='WARNING')
    parser.add_argument('--library-report', action='store_true',
                        help="If exist, creates HTML file with full library report.", default=False)
    parser.add_argument('--duplicate-report', action='store_true',
                        help="If exist, creates HTML file with duplicate report (if the duplicate search results "
                             "exits in the database.",
                        default=False)
    parser.add_argument("--dr-filename", help="Filename for duplicate report.", default="duplicate_report.html")
    parser.add_argument("--dropdb", help="True  will erase existing tables in the database.",
                        action='store_true', default=False)
    args = parser.parse_args()
    # проверка, что при read-only операциях с БД файл БД существует
    if not os.path.isfile(args.db) and args.path is False:
        print(f'Файл БД {args.db} не найден. Проверьте входные параметры.')
        sys.exit(1)
    # проверка, присутствует ли во входном имени файла БД префикс типа БД
    db_file_name = args.db
    start_dir = args.path
    log_level = log_level_select(args.loglevel)
    if args.log is False:
        logging.basicConfig(format='%(levelname)s:%(filename)s\t[LINE:%(lineno)d]:\t%(message)s', level=log_level)
    else:
        if os.path.isfile(args.log):
            os.remove(args.log)
        logging.basicConfig(format='%(levelname)s:%(filename)s\t[LINE:%(lineno)d]:\t%(message)s', filename=args.log,
                            level=log_level)
    print('Используемая база данных: {0}'.format(db_file_name))
    logging.warning("-----НАЧАЛО " + str(datetime.now()))
    logging.info(f"Используемая база данных: {db_file_name}")
    # создание объекта для доступа к БД
    db_obj = dbfunc.DbHandler(db_file_path=db_file_name, drop=args.dropdb, readonly=False)
    start_time = datetime.now()
    # гланый поток работ
    try:
        is_job_done = False
        if args.path is not False:
            print("scanning started...")
            start_dir = os.path.expanduser(start_dir)
            logging.warning("Path: {0}".format(start_dir))
            if not os.path.exists(start_dir):
                print(f'Папка {start_dir} не существует. Завершение работы.')
                sys.exit(1)
            filefunc.FileSearchThread(db_obj, start_dir)
            final_str = "scanning ended!"
            print("\r" + final_str + " " * (79 - len(final_str)))
            is_job_done = True
        if args.duplicate_report is not False:
            generatehtml.generate_compare_report(db_name=db_file_name)
            is_job_done = True
        if args.library_report is not False:
            generatehtml.generate_html_library(db_obj)
            is_job_done = True
        if is_job_done is not True:
            print("Nothing to do - please check script's input parameters. Exit.")
            logging.warning("Nothing to do - check params. Exit.")
    finally:
        db_obj.finish()  # всегда закрываем соединение с БД
        logging.info('Соединение с базой данных закрыто.')
        total_time = datetime.now() - start_time
        print(total_time)
        logging.info(str(total_time))
        logging.info('{:-^30}'.format('ЗАВЕРШЕНО'))


if __name__ == "__main__":
    main()
