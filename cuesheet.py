import logging
import os

try:
    import mutagen
except ModuleNotFoundError:
    logging.critical('module "mutagen" not found')
    import sys
    sys.exit(1)

FRAMES_PER_SECOND = 75  # magic number - see http://wiki.hydrogenaud.io/index.php?title=Cuesheet#Most_often_used


class CuesheetParsingException(Exception):
    def __init__(self, cue, audio_file=''):
        self.error_cue = cue
        self.error_audio_file = audio_file


class NotSupportedCuesheetEncoding(CuesheetParsingException):
    pass


class CuesheetReferenceAudioFileNotFound(CuesheetParsingException):
    pass


class CueSheetReferenceAudioFileCorrupted(CuesheetParsingException):
    pass


def convert_index_to_frames(index):
    frames = {}
    expstr = index.split(':')
    frames['FF'] = int(expstr[2])
    frames['SS'] = int(expstr[1]) * FRAMES_PER_SECOND
    frames['MM'] = int(expstr[0]) * 60 * FRAMES_PER_SECOND  # 60 - magic number - count of seconds in one minute
    total_frames = frames['MM'] + frames['SS'] + frames['FF']
    return total_frames


def get_seconds_form_index(index):
    frames = {}
    expstr = index.split(':')
    frames['SS'] = int(expstr[1])
    frames['MM'] = int(expstr[0]) * 60
    frames_len = 1 if int(expstr[2]) > 35 else 0  # round up frames to one second if frames is bigger than 75/2
    point_in_seconds = frames['SS'] + frames['MM'] + frames_len
    return point_in_seconds


def get_tags_from_cue_line(line):
    s = ''
    line = line.split(" ")
    if (line[0] == 'REM') and (len(line) > 2):
        if line[1] == 'GENRE':
            for a in line[2:]:
                s += a + ' '
            tagtype = 'genre'
        elif line[1] == 'DATE':
            s = line[2]
            tagtype = 'date'
        elif line[1] == 'DISCID':
            s = line[2]
            tagtype = 'discid'
        elif line[1] == 'COMMENT':
            for a in line[2:]:
                s += a + ' '
            tagtype = 'comment'
        else:
            for a in line[1:]:
                s += a + ' '
            tagtype = 'unknown'
    elif line[0] == 'PERFORMER':
        for a in line[1:]:
            s += a + ' '
        tagtype = 'artist'
    elif line[0] == 'TITLE':
        for a in line[1:]:
            s += a + ' '
        tagtype = 'title'
    elif line[0] == 'FILE':
        for a in line[1:-1]:
            s += a + ' '
        tagtype = 'file'
    elif line[0] == 'TRACK':
        for a in line[1:]:
            s += a + ' '
        tagtype = 'cuetnumber'
    elif line[0] == 'INDEX':
        for a in line[-1:]:
            s += a + ' '
        tagtype = 'start'
    else:
        tagtype = 'unknown'
        s = line[1]
    return [tagtype, s.strip().replace('"', '')]


class Cuesheet(object):
    def __init__(self, full_cue_path: str, fix_album_tag=False):
        """
        Объект cuesheet
        :type fix_album_tag: Boolean - определяет необходимость замены тега "title" на "album" в списке тегов всего
        альбома ("sharedtags")
        """
        self.sharedtags = {}
        self.tracks = []
        self.cuefilename = full_cue_path  # save original file name
        self.bitrate = 0
        self.totallength = 0
        with open(self.cuefilename, 'r', encoding='utf-8', errors='replace') as cue_file_object:
            self.cue_text = cue_file_object.readlines()
        try:
            self.parse_cue()
        except NotSupportedCuesheetEncoding:
            raise CuesheetParsingException
        self.calculate_length()
        if fix_album_tag:
            self.sharedtags['album'] = self.sharedtags.pop('title')

    def add_shared_tag(self, key, value):
        self.sharedtags[key] = value

    def add_track(self, track):
        self.tracks.append(track)

    def pprint(self):
        print('\tALBUM DATA:')
        print(self.sharedtags)
        print('\tTRACKS:')
        for i in self.tracks:
            print(i)

    def get_list_of_filenames(self):
        array_of_filenames = []
        for track in self.tracks:
            if track['file'] not in array_of_filenames:
                array_of_filenames.append(track['file'])
        return array_of_filenames

    def parse_cue(self):
        istrack = False
        is_file = False
        track, indexes, media_file_info = {}, {}, {}
        tracknumber = 0
        media_file_name, _filename = '', ''
        for line in self.cue_text:
            line = line.strip()
            result = get_tags_from_cue_line(line)
            if line.startswith("FILE"):
                _filename = result[1]  # save reference to data file to variable
                media_file_name = os.path.join(os.path.dirname(self.cuefilename), _filename)
                if os.path.exists(media_file_name) is False:
                    logging.error(f"Не найден файл {media_file_name} из cue в папке.")
                    raise CuesheetReferenceAudioFileNotFound(self.cuefilename, media_file_name)
                else:
                    filesize = os.path.getsize(media_file_name)
                try:
                    audio_file = mutagen.File(media_file_name, easy=True)  # пробуем открыть файл, чтобы получить
                    # технические характеристики аудиофайла
                    self.totallength = audio_file.info.length
                    bitrate = (filesize * 8) / audio_file.info.length / 1000
                    media_file_info[media_file_name] = {'filesize': filesize,
                                                        'mime': audio_file.mime[0],
                                                        'bitrate': bitrate}
                except Exception as e:
                    logging.error(f"Ошибка открытия файла: {media_file_name} для получения технических характеристик "
                                  f"в mutagen. Описание ошибки: {str(e)}")
                    self.totallength = 0
                    media_file_info[media_file_name] = {'filesize': 0,
                                                        'mime': 'unknown',
                                                        'bitrate': 0}
                if is_file is False:
                    is_file = True
                    track[result[0]] = result[1]
                    track['cuefilename'] = self.cuefilename
                    track['mime'] = media_file_info[media_file_name]['mime']
                    track['bitrate'] = media_file_info[media_file_name]['bitrate']
                    track['filesize'] = media_file_info[media_file_name]['filesize']
                else:
                    track['indexes'] = indexes
                    self.add_track(track)
                    track = {}
                    indexes = {}
                    istrack = False
                    track['file'] = _filename  # put reference filename to track information
                    track['cuefilename'] = self.cuefilename
                    track[result[0]] = result[1]
                    track['mime'] = media_file_info[media_file_name]['mime']
                    track['bitrate'] = media_file_info[media_file_name]['bitrate']
                    track['filesize'] = media_file_info[media_file_name]['filesize']
            elif line.startswith("TRACK"):
                if istrack is False:
                    istrack = True
                    track[result[0]] = result[1]
                    track['cuefilename'] = self.cuefilename
                else:
                    track['indexes'] = indexes
                    self.add_track(track)
                    track = {}
                    indexes = {}
                    track['file'] = _filename  # put reference filename to track information
                    track['cuefilename'] = self.cuefilename
                    track[result[0]] = result[1]
                tracknumber += 1
                track['tracknumber'] = tracknumber
                track['mime'] = media_file_info[media_file_name]['mime']
                track['bitrate'] = media_file_info[media_file_name]['bitrate']
                track['filesize'] = media_file_info[media_file_name]['filesize']
            elif istrack is True:
                if line.startswith('INDEX'):
                    s = line.split(' ')
                    indexes[s[1]] = s[2]
                else:
                    # debug
                    try:
                        track[result[0]] = result[1]
                    except:
                        try:
                            track[result[0]] = result[1]
                        except:
                            raise NotSupportedCuesheetEncoding
            else:
                self.add_shared_tag(result[0], result[1])
        track['indexes'] = indexes  # todo - possible data after "FILE" block, need to check for EOF
        self.add_track(track)
        return True

    def calculate_length(self):
        for track in self.tracks:  # check every track for missing track number key
            if 'tracknumber' not in track:
                track['tracknumber'] = 0
        # sort desc order by track number for correct calculation of length - from bigger track number sub smaller
        sorted_list = sorted(self.tracks, key=lambda k: k['tracknumber'], reverse=True)
        end = self.totallength  # working only with one audio file in cuesheet
        for i in sorted_list:
            # simple, without index 00
            try:
                index = i['indexes']['01']
                start = get_seconds_form_index(index)
                i['length'] = end - start
                end = start
            except Exception as e:
                logging.warning("Cuesheet parsing error. Calculate length failed at " + self.cuefilename +
                                '. Error: ' + str(e))
                break
        # reverse list back by tracknumber
        self.tracks = sorted(sorted_list, key=lambda k: k['tracknumber'], reverse=False)
