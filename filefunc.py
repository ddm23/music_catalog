﻿import logging
import os
import os.path
import re
import sys

import mutagen

import cuesheet
from classes.hash_object import HashObject
from classes.media_object import MediaObject
from constants import EXCLUDE_EXTENSIONS
from cuesheet import Cuesheet


def a_get_track_number(file) -> int:
    """return number of track or 0.
    splits and return first number from string like "1/10"
    """
    if 'tracknumber' in file:
        if file['tracknumber'][0] == '':
            return 0
        elif file['tracknumber'].count("/") > 0:
            line = file['tracknumber'][0].split("/")
            try:
                return int(line[0])
            except ValueError:
                return 0
        else:
            try:
                return int(file['tracknumber'][0])
            except ValueError:
                return 0
    else:
        return 0


def a_get_tag(obj, tag):
    texttags = ['title', 'artist', 'album']
    numbertags = ['date']
    if tag in texttags:
        result = obj[tag][0] if tag in obj else ''
    elif tag in numbertags:
        result = obj[tag][0] if tag in obj else 0
    else:
        result = False
    return result


def try_to_open(path):
    logging.debug("Попытка открыть файл {0}".format(path))
    try:
        file = mutagen.File(path, easy=True)
        if file is not None:
            return file
        else:
            logging.info("Не медиа файл: {0}".format(path))
            return False
    except Exception as e:
        logging.error("Ошибка открытия файла: {0} - '{1}'.".format(str(e), path))
        return False


class FileSearchThread(object):
    def __init__(self, db, dirforstart):
        self.db_obj = db
        self.pattern = re.compile("(\\b|\\s)(of|the|for|&|and|feat.|feat|a)(\\s|\\b)", re.I)
        self.recursively_add_files_to_db(dirforstart)

    def get_optimized_for_search_string(self, string: str) -> str:
        a = self.pattern.sub("", string).strip()
        return a

    def recursively_add_files_to_db(self, current_path):
        try:
            # just progress bar - current folder's name
            folder = os.path.basename(os.path.normpath(current_path))
            sys.stdout.write("\r" + folder[:79] + " " * (79 - len(folder)))
            sys.stdout.flush()
        except:
            pass
        # get list of dirs and files from the path
        temp_list = os.listdir(current_path)
        dirs = []
        cuelist = []
        files = []
        # check every entity in result
        for entity in temp_list:
            # if it is a dir
            if os.path.isdir(os.path.join(current_path, entity)):
                dirs.append(os.path.join(current_path, entity))
            # if it is file
            else:
                # if it a cue sheet
                if entity.lower().endswith('.cue') and not entity.startswith("."):
                    cuelist.append(entity)
                # if file name starts with '.' or it is not cue sheet, but totally not audio file
                elif entity.startswith(".") or entity.lower().endswith(EXCLUDE_EXTENSIONS):
                    # ignore it
                    pass
                else:
                    # probably, it's audio file, add to further process
                    files.append({'original': entity, 'lowercase': entity.lower()})

        # process all cue sheets
        if len(cuelist) > 0:
            if len(cuelist) > 1:
                logging.error(f"Обнаружено больше одного файла cuesheet в папке {current_path}. "
                              f"Обработан будет только первый корректный файл cue, остальные будут проигнорированы")
            for cue_file in cuelist:
                cue_is_correct = True  # believe all is correct in cue sheet
                logging.info('Проверка cuesheet {0}'.format(os.path.join(current_path, cue_file)))
                try:
                    cue_object = Cuesheet(os.path.join(current_path, cue_file))
                    cue_audio_files_list = cue_object.get_list_of_filenames()  # get list of referenced
                    # audio files from the cue sheet
                except (cuesheet.NotSupportedCuesheetEncoding, cuesheet.CuesheetReferenceAudioFileNotFound):
                    cue_is_correct = False
                # если из cue получилось извлечь список связанных аудиофайлов, начинается проверка каждого
                # на наличие в папке
                if cue_is_correct is True and len(cue_audio_files_list) > 0:
                    for file_from_cue in cue_audio_files_list:
                        current_cue_file_founded = False
                        for file in files:  # check every file in directory
                            if file_from_cue.lower() == file['lowercase']:
                                current_cue_file_founded = True
                        if current_cue_file_founded is False:
                            cue_is_correct = False  # if at least one file was not founded, the cue sheet is excluding
                            logging.error("Файл '{0}' не обнаружен в папке.".format(file_from_cue))
                            break
                else:
                    cue_is_correct = False
                # если все перечисленные файлы в cue находятся в папке, удаляем их из списка аудиофайлов,
                # чтобы не грузить как дубли в базу данных
                if cue_is_correct is True:
                    logging.debug('Cue sheet is correct.')
                    for file_from_cue in cue_audio_files_list:
                        for file in files:
                            try:
                                if file_from_cue.lower() == file['lowercase']:
                                    i = files.index(file)  # search index of file from the cue sheet
                                    del files[i]  # delete file name from the file list
                            except:
                                logging.error(f"Ошибка удаления файла '{file}' из папки.")
                    self.load_tracks_from_cue_sheet_to_db(cue_obj=cue_object,
                                                          root=current_path,
                                                          input_file_name=cue_file)
                    break  # оставшиеся cue игнорируются
                else:
                    logging.error(
                        f"Cuesheet '{os.path.join(current_path, cue_file)}' некорректна и была проигнорирована.")
            logging.debug('Все cuesheet в папке обработаны.')

        # add regular audio files to database
        if files:
            for filename in files:
                self.load_file_to_db(current_path, filename['original'])

        # recursively go to subdirectories
        if dirs:
            for i in dirs:
                self.recursively_add_files_to_db(i)

    def load_file_to_db(self, root, filename):
        """
        The function prepares and load input file to database
        :param root: path to file
        :param filename: filename with extension
        """
        path = os.path.join(root, filename)
        file = try_to_open(path)
        if file is not False:
            file_size = os.path.getsize(path)
            try:
                bitrate = file.info.bitrate / 1000
            except:
                try:
                    bitrate = (file_size * 8) / file.info.length / 1000
                except:
                    bitrate = 0
            instance = MediaObject(artist=a_get_tag(file, 'artist'),
                                   title=a_get_tag(file, 'title'),
                                   tnumber=a_get_track_number(file),
                                   album=a_get_tag(file, 'album'),
                                   date=a_get_tag(file, 'date'),
                                   length=int(file.info.length),
                                   path=root,
                                   file=filename,
                                   bitrate=int(bitrate),
                                   mime=file.mime[0],
                                   filesize=file_size)
            self.add_media_object(instance)
            logging.debug("File added to database.")

    def load_tracks_from_cue_sheet_to_db(self, cue_obj, root, input_file_name):
        try:
            common_artist_tag = cue_obj.sharedtags['artist'] if 'artist' in cue_obj.sharedtags else ''
            album = cue_obj.sharedtags['title'] if 'title' in cue_obj.sharedtags else ''
            date = cue_obj.sharedtags['date'] if 'date' in cue_obj.sharedtags else 0
            for track in cue_obj.tracks:
                try:
                    tnumber = track['tracknumber']
                except KeyError:
                    tnumber = 0
                instance = MediaObject(artist=track['artist'] if 'artist' in track else common_artist_tag,
                                       title=track['title'] if 'title' in track else '',
                                       tnumber=tnumber,
                                       album=album,
                                       date=date,
                                       length=int(track['length']) if 'length' in track else '0',
                                       path=root,
                                       file=track['file'] if 'file' in track else input_file_name,
                                       bitrate=int(track['bitrate']) if 'bitrate' in track else 0,
                                       mime=track['mime'] if 'mime' in track else 'unknown',
                                       filesize=track['filesize'] if 'filesize' in track else 0,
                                       path_to_cue=os.path.join(root, input_file_name))
                self.add_media_object(instance)
            logging.debug(f"Файл добавлен из cue sheet {os.path.join(root, input_file_name)}.")
        except cuesheet.NotSupportedCuesheetEncoding:
            logging.error(f"Файл cuesheet {os.path.join(root, input_file_name)} имеет неподдерживаемую кодировку.")
        except cuesheet.CuesheetReferenceAudioFileNotFound:
            pass

    def add_media_object(self, mo: MediaObject) -> None:
        hashes = [HashObject(str_for_hashing=str(mo.artist.lower() + mo.title.lower()), hash_type='ar_tt'),
                  HashObject(str_for_hashing=str(mo.album.lower() + mo.title.lower()), hash_type='al_tt')
                  ]
        if mo.path_to_cue is None:
            hashes.append(HashObject(str_for_hashing=str(mo.filesize), hash_type='fs'))
        self.db_obj.insert_new_media_object_to_db(mo, hashes)
