import argparse
import subprocess
from random import randint

from dbfunc import DbHandler

parser = argparse.ArgumentParser()
parser.add_argument("--db",
                    help="Filename of the database. 'sqlite:///:memory:' for in-memory sqlite3 mode. Default is "
                         "'database.db'.",
                    default='database.db')
args = parser.parse_args()
db_obj = DbHandler(db_file_path=args.db, drop=False, readonly=True)
count = db_obj.get_library_items_count()
item = randint(1, count)
line = db_obj.get_library_item(item)
out = f'{line[1]} {line[3]} {line[2]}'
print(out)
process = subprocess.Popen(
        'pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
process.communicate(out.encode('utf-8'))
