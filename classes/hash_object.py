class HashObject:
    """
    Класс для хранения структуры хэша записи
    Полученное значение str_for_hashing преобразуется в хэш в поле hashed
    С помощью метода from_db() можно создать запись с уже посчитанным хэшем и связкой на ext_id dв БД
    """

    def __init__(self, hash_type: str, hashed: str = None, str_for_hashing: str = None, ext_id=None):
        self.str_for_hashing = str_for_hashing
        self.hashed = hashed
        self.hash_type = hash_type
        self.ext_id = ext_id
        if self.str_for_hashing is not None and self.hashed is None:
            self.hashed = hash(self.str_for_hashing)

    @classmethod
    def from_db(cls, ext_id, hashed, hash_type) -> 'HashObject':
        return cls(ext_id=ext_id, hashed=hashed, hash_type=hash_type)

    def pprint(self):
        print(f'Hashed: {self.hashed} as {self.hash_type}')