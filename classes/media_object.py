class MOContaner:
    """
    Контейнер под объекты класса MediaObject
    """
    def __init__(self):
        self.ids = set()
        self.mo_objs = list()

    def __iter__(self):
        return iter(self.mo_objs)

    def add(self, dict_: dict, id_):
        self.mo_objs.append(MediaObject(**dict_))
        self.ids.add(id_)

    def has_same_ids(self, mo_ids: set) -> bool:
        """Проверка наличия отличающихся ID записей MediaObject. Если разница между множествами ненулевая,
        множества различаются."""
        if len(self.ids.difference(mo_ids)) == 0:
            return True

    def get_mo_count(self):
        return len(self.mo_objs)

    @property
    def get_ids(self):
        return self.ids


class MediaObject:
    """
    Как твоя бывшая - запихнет все, что предложишь, и не вернет назад.
    """
    def __init__(self, **kwargs):
        self.metadata = kwargs

    def __getattr__(self, item):
        try:
            return self.metadata[item]
        except KeyError:
            return None

    def __getitem__(self, item):
        return self.metadata[item]

    def get_value_for_compare(self, attr):
        if attr in self.metadata:
            if type(self.metadata) is str:
                return self.metadata[attr].lower()
            else:
                return self.metadata[attr]
        else:
            return None

    def pprint(self):
        print(self.metadata)

    def get_keys(self):
        return self.metadata.keys()
