import argparse
import ftplib
import os
import sys
from signal import signal, SIGPIPE, SIG_DFL

import cuesheet
import splitfilebycue
from commonfunc import load_dict_from_config
from constants import MEDIA_EXTENSIONS, EXCLUDE_EXTENSIONS, INCLUDE_FILENAMES
from cuesheet import Cuesheet

DIFF_LIMIT_TO_UPLOAD = 0.1  # если разница между размерами файла в процентах меньше указанного, загрузка файла
# не производится


class FolderTree(object):
    def __init__(self, path: str):
        self.root_path = path
        self.result_tree = []
        self.collect_tree()
        self.clean_tree()
        self.find_and_split_cue()

    def collect_tree(self) -> None:
        """
        Функция сбора все файлов и подпапок указанной на вход папки
        :return: список в виде dict
        """
        # Первый – адрес каталога, второй – список его поддиректорий первого уровня, третий – список имен файлов.
        fin = os.walk(self.root_path)
        for i in fin:
            self.result_tree.append(dict(path=i[0], subfolders=i[1], files=i[2], to_delete=False))

    def clean_tree(self) -> None:
        """
        Очищает полученный лист папок и файлов от не относящихся к медиафайлам
        """
        changes = True
        while changes != False:
            changes = False
            for node in self.result_tree:
                if node['to_delete'] is False:
                    node_for_remove = False
                    media_files_check_result = self.check_for_media_files(node['files'])
                    if len(node['subfolders']) == 0 and media_files_check_result is False:
                        node_for_remove = True
                    elif len(node['files']) > 0 and not node_for_remove:
                        files_to_check = []
                        for file in node['files']:
                            if file.startswith('.') or (
                                    file.lower().endswith(EXCLUDE_EXTENSIONS) and
                                    not file.lower().startswith(INCLUDE_FILENAMES)):
                                changes = True
                            else:
                                files_to_check.append(file)
                        node['files'] = files_to_check

                    # необходимо удалить всю папку, включая ссылку в родителе
                    if node_for_remove:
                        node['to_delete'] = True
                        node_key = os.path.basename(os.path.normpath(node['path']))
                        parent_key = os.path.abspath(os.path.join(node['path'], os.pardir))
                        for parent in self.result_tree:
                            if parent['path'] == parent_key or parent['path'][:-1] == parent_key:
                                try:
                                    parent['subfolders'].remove(node_key)
                                except KeyError:
                                    print(f"Подпапка {node_key} отсутствует у родителя {parent_key}")
                                break
                        changes = True
        for i in self.result_tree:
            files = dict()
            for file in i['files']:
                files[file] = (os.path.join(i['path'], file))
            i['files'] = files

    def find_and_split_cue(self) -> None:
        for item in self.result_tree:
            for file in [*item['files'].keys()]:  # итерируемся по имени файла
                if file.lower().endswith('.cue'):  # если кончается на кую - наш пациент
                    path_to_cue = item['files'][file]  # получаем полный путь до куи
                    try:
                        cue = Cuesheet(path_to_cue)  # открываем как объект класса
                    except cuesheet.CuesheetReferenceAudioFileNotFound:
                        pass
                    else:
                        image_files_list = cue.get_list_of_filenames()  # получаем список всех вложенных файлов
                        if len(image_files_list) == 1 \
                                and len(cue.tracks) > 1 \
                                and image_files_list[0].endswith(('.flac', '.ape')):  # todo - сделать параметром
                            # если файл образа один, и внутри содержит несколько треков - его надо разрезать
                            # todo - проверяем наличие файлов из cue в списке файлов в папке
                            try:
                                replace_files = splitfilebycue.split_file_by_cue(
                                    path_to_cue, return_files_paths=True)  # вызываем разрезание файла и получаем
                                # список полученных файлов
                            except splitfilebycue.SplittedFilesListIsEmpty:
                                print("Некорретный результат разрезания по cue.")
                            else:
                                item['files'].pop(image_files_list[0])
                                for new_file in replace_files:  # для каждого файла из разрезанных
                                    item['files'][os.path.basename(new_file)] = new_file
                    item['files'].pop(file)  # удаляем саму .cue

    def get_tree(self):
        return self.result_tree

    @staticmethod
    def check_for_media_files(files: list) -> bool:
        if len(files) > 0:
            for file in files:
                if file.lower().endswith(MEDIA_EXTENSIONS):
                    return True
        return False


class ProgressBar(object):
    def __init__(self, size):
        self.max = size
        self.current = 0
        self.percent = 0

    def update(self, buf):  # buf нигде не используется, требуется для корректной отработки callback
        if self.max > 8192:
            self.current += 8192
        else:
            self.current = self.max
        self.percent = int((100 * self.current) / self.max)
        bars = '#' * round((self.percent / 10))
        sys.stdout.write("\r[{1} {0} %]".format(self.percent, bars))
        sys.stdout.flush()

    @staticmethod
    def finish():
        print("\n")


def upload_to_server(server, path_list):
    with ftplib.FTP() as ftp:
        ftp.encoding = 'UTF-8'
        try:
            ftp.connect(host=server['HOST'], port=server['PORT'], timeout=30)
            ftp.login()
        except Exception as e:
            print(f'{server} connection problem: {e}')
        # добираемся до каталога-хранилища файлов
        if 'start' in server:
            if len(server['start']) > 0:
                try:
                    ftp.cwd(server['start'])
                except EOFError as e:
                    print(f'{server} default folder opening problem: {e}')
        for path in path_list:
            # проверяем отсутствие загружаемой папки, если отсутствует - создаем
            folder = os.path.basename(os.path.normpath(path))
            data = ftp.nlst()
            folder_is_exists = False
            if folder not in data:
                ftp.mkd(folder)
            else:
                folder_is_exists = True  # Признак того, что папка существует
            ftp.cwd(folder)
            upload_tree = FolderTree(path)
            upload_recursive(ftp=ftp,
                             upload_folder_object=upload_tree.result_tree,
                             current_key=path,
                             folder_is_exists=folder_is_exists)  # возврат в папку хранилища происходит в рамках функции


def upload_recursive(ftp, upload_folder_object: list, current_key, folder_is_exists=False):
    print(f'начало загрузки папки {current_key}')
    ftp_dir_files = []  # иницииация переменной для хранения списка файлов каталога на сервере
    folders, files, files_keys = list, list, list
    if folder_is_exists:
        ftp_dir_files = ftp.nlst()  # если папка уже есть на сервере, получаем список всех файлов в ней
    for item in upload_folder_object:
        if item['path'] == current_key and item['to_delete'] is False:
            folders, files, files_keys = item['subfolders'], item['files'], [*item['files'].keys()]
    for file in files_keys:
        size = os.path.getsize(files[file])  # размер локального файла
        if folder_is_exists:
            if file in ftp_dir_files:
                size_ftp = ftp.size(file)  # получение размера файла на сервере
                if size_ftp == 0:  # проверка, поддерживается ли функция SIZE на сервере
                    print(f'функция SIZE не поддерживается. пропускаем файл {file}')
                    continue
                else:
                    diff = (size - size_ftp) / 100
                    if diff <= DIFF_LIMIT_TO_UPLOAD:
                        print(f"размер файла {file} в пределах погрешности. пропускаем.")
                        continue
        print(f"начинается загрузка файла {file}")
        pbar = ProgressBar(size)
        try:
            with open(files[file], 'rb') as fh:
                try:
                    ftp.storbinary('STOR %s' % file, fh, callback=pbar.update)
                except ftplib.error_perm as e:
                    pbar.finish()
                    print(f"Ошибка при загрузке файла {file}: {str(e)}")
                    sys.exit(1)
            pbar.finish()
        except ftplib.socket.timeout:
            print("Таймаут при загрузке файла.")
            pbar.finish()

    # после загрузки всех файлов в папке начинается загрузка вложенных папок
    for folder in folders:
        if folder_is_exists:  # если папка уже была на сервере, делаем проверку наличия создаваемой подпапки
            if folder in ftp_dir_files:
                subfolder_is_exists = True
            else:
                subfolder_is_exists = False
        else:
            subfolder_is_exists = False

        if not subfolder_is_exists:
            try:
                print(f"создаем вложенную папку {folder}")
                ftp.mkd(folder)
                subfolder_is_exists = False
            except (ftplib.error_perm, EOFError):
                print(f'folder {folder} already exists')
                subfolder_is_exists = True
        ftp.cwd(folder)
        upload_recursive(ftp=ftp,
                         upload_folder_object=upload_folder_object,
                         current_key=os.path.join(current_key, folder),
                         folder_is_exists=subfolder_is_exists)
    ftp.cwd('..')
    os.chdir('..')
    print(f"загрузка папки {current_key} завершено")


def check_connection_to_ftp_server(servers: dict) -> list:
    """
    Проверка доступности FTP серверов (соединение, авторизация)
    :param servers: список серверов для проверки доступности
    :return: list - идентификаторы доступных серверов
    """
    online = []
    for i in servers:
        with ftplib.FTP() as ftp:
            try:
                ftp.connect(host=servers[i]['HOST'], port=servers[i]['PORT'], timeout=5)
            except:
                continue
            try:
                ftp.login()
            except:
                continue
            online.append(i)
    return online


def main():
    signal(SIGPIPE, SIG_DFL)  # непонятная хуйня, но должна убирать ошибки, связанные с FTP
    parser = argparse.ArgumentParser()
    parser.add_argument("paths", action="store",
                        nargs="*",
                        help="Paths for uploading to FTP servers.")
    args = parser.parse_args()
    items_list = args.paths
    folders_to_upload = []
    files_to_upload = []
    # сортируем найденное на файлы и папки
    for item in items_list:
        if os.path.isfile(item):
            if not item.startswith('.'):  # отбрасываем скрытые файлы, имя которых начинается с точки
                files_to_upload.append(item)
        elif os.path.isdir(item):
            folders_to_upload.append(item)

    if len(folders_to_upload) == 0 and len(files_to_upload) == 0:
        print('Nothing to upload. Exit.')
        sys.exit(0)
    servers = load_dict_from_config(config_name='uploadtoftpservers')
    online_servers = check_connection_to_ftp_server(servers)
    print("Target FTP servers:")
    for i in servers:
        if i in online_servers:
            status = "ONLINE"
        else:
            status = "OFFLINE"
        print(f'{i} ({servers[i]["HOST"]}:{servers[i]["PORT"]}): {status}')
    # if len(SERVERS) != len(online_servers):
    #     print("--------------------------")
    #     answer = input("Some servers is offline. Continue? y/n  ")
    #     if answer is not 'y':
    #         sys.exit(0)
    for i in online_servers:
        print(f"начало загрузки на устройство {i}")
        upload_to_server(servers[i], folders_to_upload)
    print(f"загрузка папок завершена")


if __name__ == "__main__":
    main()
