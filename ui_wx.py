import sys

import wx
import wx.adv

from dbfunc import DbHandler

VERSION = '0.1'
PROJECT_LINK = 'https://bitbucket.org/ddm23/music_catalog/src/master/'


class MainGridPanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)
        self.db_file_path = ''
        self._offset = 0
        self._limit = 30
        self.data = []
        self._max_library_item = 0
        self.db_object = None
        # добавление главного сайзера
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        # объявление сайзера, отвечающего за лист
        self.list_ctrl = wx.ListCtrl(
            self, id=wx.ID_ANY,
            style=wx.LC_REPORT | wx.LC_VRULES | wx.LC_HRULES | wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL
        )
        self.list_ctrl.InsertColumn(0, '#', width=40)
        self.list_ctrl.InsertColumn(1, 'Artist')
        self.list_ctrl.InsertColumn(2, 'Date', width=45)
        self.list_ctrl.InsertColumn(3, 'Album')
        self.list_ctrl.InsertColumn(4, 'Title')
        self.list_ctrl.InsertColumn(5, 'T\\N', width=wx.LIST_AUTOSIZE_USEHEADER)
        self.list_ctrl.InsertColumn(6, 'Codec')
        self.list_ctrl.InsertColumn(7, 'Bitrate', width=wx.LIST_AUTOSIZE_USEHEADER)
        self.list_ctrl.InsertColumn(8, 'Path')
        self.need_to_resize = [0, 1, 3, 4, 6, 8]

        main_sizer.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 5)

        # создать сайзер для нижних кнопок
        bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)
        navigation_sizer = wx.BoxSizer(wx.HORIZONTAL)

        first_page_button = wx.Button(self, id=wx.ID_FIRST, label='<<', size=wx.Size(40, -1))
        first_page_button.Bind(wx.EVT_BUTTON, self.get_first_page_of_library_items)
        navigation_sizer.Add(first_page_button, 0, wx.TOP | wx.BOTTOM, 5)

        prev_page_button = wx.Button(self, id=wx.ID_BACKWARD, label='<', size=wx.Size(40, -1))
        prev_page_button.Bind(wx.EVT_BUTTON, self.get_prev_page_of_library_items)
        navigation_sizer.Add(prev_page_button, 0, wx.ALL, 5)

        next_page_button = wx.Button(self, id=wx.ID_FORWARD, label='>', size=wx.Size(40, -1))
        next_page_button.Bind(wx.EVT_BUTTON, self.get_next_page_of_library_items)
        navigation_sizer.Add(next_page_button, 0, wx.ALL, 5)

        last_page_button = wx.Button(self, id=wx.ID_LAST, label='>>', size=wx.Size(40, -1))
        last_page_button.Bind(wx.EVT_BUTTON, self.get_last_page_of_library_items)
        navigation_sizer.Add(last_page_button, 0, wx.ALL, 5)

        self.items_counter = wx.StaticText(self, id=wx.ID_ANY, label=f'{self._offset}/{self._max_library_item}')
        navigation_sizer.Add(self.items_counter, 0, wx.CENTER, 5)

        exit_button = wx.Button(self, label='Exit')
        exit_button.Bind(wx.EVT_BUTTON, on_exit)

        bottom_sizer.Add(navigation_sizer, 1, wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 0)
        bottom_sizer.Add(exit_button, 0, wx.ALL | wx.ALIGN_RIGHT, 5)
        main_sizer.Add(bottom_sizer, 0, wx.EXPAND)
        self.SetSizer(main_sizer)

    def update_list_ctrl_by_data(self):
        # обновить счетчик текущей страницы
        self.items_counter.SetLabel(f'{self._offset}/{self._max_library_item}')
        # удалить все ранее загруженные результаты
        self.list_ctrl.DeleteAllItems()
        # загрузить новые результаты из data
        for item in self.data:
            index = self.list_ctrl.InsertItem(self.list_ctrl.GetItemCount(), item[0])
            for text in enumerate(item):
                self.list_ctrl.SetItem(index=index, column=text[0], label=str(text[1]))
        # изменить ширину выбранные столбцов по их значению
        for index in self.need_to_resize:
            self.list_ctrl.SetColumnWidth(index, wx.LIST_AUTOSIZE)

    def get_next_page_of_library_items(self, _):
        """
        Перезагрузка списка data следующей страницей таблицы library относительно текущих значений
        """
        self._offset += self._limit
        if self._offset > self._max_library_item:
            self._offset = (self._max_library_item // self._limit) * self._limit
        self.data = self.db_object.select_all_from_library_ordered_offset(offset=self._offset, limit=self._limit)
        self.update_list_ctrl_by_data()

    def get_prev_page_of_library_items(self, _):
        """
        Перезагрузка списка data предыдущей страницей таблицы library относительно текущих значений
        """
        self._offset -= self._limit
        if self._offset < 0:
            self._offset = 0
        self.data = self.db_object.select_all_from_library_ordered_offset(offset=self._offset, limit=self._limit)
        self.update_list_ctrl_by_data()

    def get_first_page_of_library_items(self, _=None):
        """
        Перезагрузка списка data первой страницей таблицы library относительно текущих значений
        """
        self._offset = 0
        self.data = self.db_object.select_all_from_library_ordered_offset(offset=self._offset, limit=self._limit)
        self.update_list_ctrl_by_data()

    def get_last_page_of_library_items(self, _):
        """
        Перезагрузка списка data последней страницей таблицы library относительно текущих значений
        """
        self._offset = (self._max_library_item // self._limit) * self._limit
        self.data = self.db_object.select_all_from_library_ordered_offset(offset=self._offset, limit=self._limit)
        self.update_list_ctrl_by_data()

    def init_database(self):
        # закрыть соединение с файлов БД, если оно было открыто
        if self.db_object is not None:
            self.db_object.finish()
        # удаление предыдущего объекта БД
        del self.db_object
        # подключение к БД
        self.db_object = DbHandler(db_file_path=self.db_file_path, readonly=True)
        # получение максимального значения индекса в таблице library
        self._max_library_item = self.db_object.get_library_items_count()
        # наполнить первоначальный список со старта
        self.get_first_page_of_library_items()


def on_exit(_):
    sys.exit(0)


class MainFrame(wx.Frame):
    def __init__(self):
        super().__init__(parent=None,
                         id=wx.ID_ANY,
                         title=f'Music Catalog v.{VERSION}',
                         size=wx.Size(1000, -1)
                         )
        self.panel = MainGridPanel(self)
        self.create_menu()  # вызвать создание меню
        self.Show()
        self.Maximize(True)

    def create_menu(self):
        # FILE
        file_menu = wx.Menu()  # инициация объекта меню
        menu_open = file_menu.Append(wx.ID_OPEN,
                                     "&Open Database file",
                                     " Open a database to view")
        menu_exit = file_menu.Append(wx.ID_EXIT, 'Exit', 'Exit')  # добавление в меню пункта выхода
        menu_quit = file_menu.Append(-1, 'Exit', 'Exit')  # добавление в меню второго пункта выхода,
        # чтобы он появился в меню macOs
        # ABOUT
        about_menu = wx.Menu()
        menu_about = about_menu.Append(wx.ID_ABOUT, 'About', 'About')
        menu_bar = wx.MenuBar()  # инициация объекта бара меню
        menu_bar.Append(file_menu, '&File')  # добавление в менюбар пункта с открытием папки
        menu_bar.Append(about_menu, '&About')

        self.SetMenuBar(menu_bar)  # прибить к текущему объекту ранее созданный менюбар

        self.Bind(wx.EVT_MENU, on_exit, menu_exit)
        self.Bind(wx.EVT_MENU, on_exit, menu_quit)
        self.Bind(wx.EVT_MENU, self.open_db, menu_open)
        self.Bind(wx.EVT_MENU, self.on_about, menu_about)

    def open_db(self, _):
        # открытие файла БД
        with wx.FileDialog(self, "Open SQLite database file with music catalog", wildcard="SQLite files (*.db)|*.db",
                           style=wx.FD_OPEN
                                 | wx.FD_FILE_MUST_EXIST
                           ) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return
            # обработка файла
            self.panel.db_file_path = fileDialog.GetPath()
            self.panel.init_database()
            self.SetTitle(f'Music Catalog v.{VERSION} - {fileDialog.GetFilename()}')

    @staticmethod
    def on_about(_):
        info = wx.adv.AboutDialogInfo()
        info.SetName("Music Catalog")
        info.SetVersion(VERSION)
        info.SetDescription("GUI for viewing Music Catalog local database.")
        # info.SetCopyright("(C) 1992-2012")
        info.SetWebSite(PROJECT_LINK)
        info.AddDeveloper("dd23")
        wx.adv.AboutBox(info)


if __name__ == '__main__':
    app = wx.App(False)
    frame = MainFrame()
    app.MainLoop()
