import json
import os
import sys


def load_dict_from_config(config_name: str) -> dict:
    """
    Загрузка json с параметрами из конфигурационного файла из папки configs
    :file_name: имя файла конфигурации без расширения
    :return: dict с параметрами
    """
    try:
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'configs',
                               f'{config_name}.ini'), 'r') as configfile:
            return json.load(configfile)
    except FileNotFoundError:
        print(f'Config file "{config_name}.ini" not found.')
        sys.exit(1)


def save_dict_to_config(file_name: str, saving: dict):
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           'configs',
                           f'{file_name}.ini'), 'w') as configfile:
        configfile.write(json.dumps(saving))
