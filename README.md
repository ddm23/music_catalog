# Music duplicator #
Music duplicator is python module for searching duplicates through music audio files by tags on local storage.
Based on [lazka's mutagen module](https://mutagen.readthedocs.io/en/latest/) and support all media types provided by mutagen.

*Capabilities:*

* Scans local folder for music files and puts information to SQLite database.
* Provides native UI app based on wxPython framework, and export to html.
* Can search duplicates of music files on the library.
* Upload folders to FTP multiple servers.
* Split single FLAC file by cuesheet to separated files.


Written on Python 3.
Licensed under GNU GPL v2.

# Prepare to use #
1. Install 'mutagen', 'sqlalchemy' modules. For UI install 'wxpython'.
2. Execute 'main.py' with custom arguments
3. Run 'ui_wx.py' for native app and select database to view.

# Documentation 
[https://bitbucket.org/ddm23/music_catalog/wiki/Home](https://bitbucket.org/ddm23/music_catalog/wiki/Home)

# Install shnsplit for macos
for install mac to split APE:  
*brew tap fernandotcl/homebrew-fernandotcl  
brew install monkeys-audio*