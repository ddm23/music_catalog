# модуль с общими для нескольких скриптов константами

# --- НАСТРОЙКИ

# Папка для временных файлов, создаваемых при разрезания медиафайла
split_result_output_root_path = "/Users/Dmitry/music_catalog/temp"

# --- КОНСТАНТЫ
# расширения аудио файлов
MEDIA_EXTENSIONS = ('.mp3', '.flac', '.ape', '.aac', '.wv', '.m4a', '.wma')
# расширения для исключаемых файлов. ОБЯЗАТЕЛЬНО в нижнем регистре!
EXCLUDE_EXTENSIONS = ('.jpg', '.lrc', '.jpeg', '.txt', '.png', '.!ut', '.m3u', '.dat', '.log', '.part')
# расширения для названий файлов обложек
# важно! все в нижнем регистре!
INCLUDE_FILENAMES = ("folder", 'cover', 'front')
